# A Hadoop Sandbox

提供一個 Hadoop 1.2.1的Sandbox環境來快速測試Hadoop

## Specs

目前這個Sandbox包含一個 1 Hadoop node，以pseudo-distributed mode執行。

* Linux: Ubuntu Server 14.04 LTS x64 (Trusty Tahr)
* RAM: 4GB (這個VM需要4GB，因此請確認您的機器有足夠的RAM)
* Hadoop version: 1.2.1 downloaded from hadoop.apache.org
* Java: Oracle JAVA 1.6 (***請注意！由於安裝與使用Oracle JAVA需要您同意伴隨軟體發行的license，本script包含自動同意license的程式碼，因此使用此script即代表您同意Oracle license條文內容！***
    License全文請閱「Oracle Binary Code License Agreement for Java SE」`http://www.oracle.com/technetwork/java/javase/terms/license/index.html`

## Usage

依電腦規格與網路速度的不同，第一次安裝本環境需時約15至25分鐘。

首先請先在您的電腦安裝 [VirtualBox](https://www.virtualbox.org/wiki/Downloads) 與 [Vagrant](https://www.vagrantup.com/downloads.html)。

* Vagrant請安裝1.6.5以上的版本，目前Ubuntu官方套件庫裡的的Vagrant版本太舊不建議使用。

接著在你本機任意路徑做clone repository。

```bash
$ git clone https://bitbucket.org/xylin/hadoop-1_2_1-sandbox.git
```

然後進去目錄啟動VM

```bash
$ cd hadoop-1_2_1-sandbox
$ vagrant up
```

由於是第一次開啟VM，需要安裝很多套件，依網路及電腦速度不同可能要等一陣子才會執行完成所有套件安裝工作。

看到`=> master: Done: Hadoop initialization`訊息表示VM已經provision完成

接著請用```vagrant ssh```直接SSH進去VM的shell

```bash
$ vagrant ssh
```


## Start/Stop Hadoop services
Hadoop安裝在`/opt/hadoop-1.2.1`，預設不啟動服務

若要啟動Hadoop請執行：

```bash
start-all.sh
```

第一次啟動會出現下面訊息

```
The authenticity of host 'localhost (127.0.0.1)' can't be established.
ECDSA key fingerprint is <XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX>.
Are you sure you want to continue connecting (yes/no)?
```

請輸入`yes`然後按enter繼續

開好之後使用`jps`命令查看，若有列出以下process則表示Hadoop啟動成功
```
XXXX JobTracker
XXXX TaskTracker
XXXX Jps
XXXX DataNode
XXXX NameNode
XXXX SecondaryNameNode
```

也可以從browser連上NameNode和JobTracker的web介面查看：

* NameNode - http://10.211.55.100:50070/
* JobTracker - http://10.211.55.100:50030/

要關掉Hadoop請執行：

```bash
stop-all.sh
```

## Vagrant常用指令

以下指令是在你的桌機或NB上面執行，若是先前已經用`vagrant ssh`連入VM環境，請先`$ exit`中斷SSH session。

由於一個人的電腦可能裝有很多Vagrant控制的VM，而Vagrant指令都有預設的search優先順序([參考說明](https://docs.vagrantup.com/v2/vagrantfile/))，因此建議先切換到放Vagrantfile的檔案目錄再執行以下命令。

* 開VM：`vagrant up`
* SSH連線進VM：`vagrant ssh`
* 關VM：`vagrant halt`
* 暫停VM：`vagrant suspend`
* 喚醒VM：`vagrant resume`
* 砍掉VM：`vagrant destory`



