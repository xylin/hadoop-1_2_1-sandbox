# -*- mode: ruby -*-
# vi: set ft=ruby :
$init_script = <<SCRIPT
#!/bin/bash

add-apt-repository ppa:webupd8team/java
apt-get update
export DEBIAN_FRONTEND=noninteractive
echo oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
apt-get -q -y install oracle-java7-installer
update-java-alternatives -s java-7-oracle
apt-get -q -y install oracle-java7-set-default git mongodb python-pymongo python-numpy python-sklearn python-pip python-paramiko ipython python-lxml
pip install scp
useradd -m -s /bin/bash -U itri-hadoop -G sudo 
# install pybrain
git clone git://github.com/pybrain/pybrain.git
~/pybrain/python setup.py install

cat > /etc/default/locale <<EOF
LANGUAGE=en_US:en
LANG=en_US.UTF-8
LC_ALL=en_US.UTF-8

EOF

curl -s -o /opt/hadoop-1.2.1-bin.tar.gz http://ftp.twaren.net/Unix/Web/apache/hadoop/common/hadoop-1.2.1/hadoop-1.2.1-bin.tar.gz
tar xzvf /opt/hadoop-1.2.1-bin.tar.gz -C /opt
chown -R vagrant.vagrant /opt/hadoop-1.2.1
SCRIPT

$user_script = <<SCRIPT
#!/bin/bash

echo Start: Hadoop initialization...

cat > /opt/hadoop-1.2.1/conf/core-site.xml <<EOF
<?xml version="1.0"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>

<!-- Put site-specific property overrides in this file. -->

<configuration>
     <property>
         <name>fs.default.name</name>
         <value>hdfs://localhost:9000</value>
     </property>
</configuration>
EOF

cat > /opt/hadoop-1.2.1/conf/hdfs-site.xml <<EOF
<?xml version="1.0"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>

<!-- Put site-specific property overrides in this file. -->

<configuration>
     <property>
         <name>dfs.replication</name>
         <value>1</value>
     </property>
</configuration>
EOF

cat > /opt/hadoop-1.2.1/conf/mapred-site.xml <<EOF
<?xml version="1.0"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>

<!-- Put site-specific property overrides in this file. -->

<configuration>
     <property>
         <name>mapred.job.tracker</name>
         <value>localhost:9001</value>
     </property>
</configuration>
EOF

cat >> /opt/hadoop-1.2.1/conf/hadoop-env.sh <<EOF
export JAVA_HOME=/usr/lib/jvm/java-7-oracle

EOF

cat >> ~/.bashrc <<EOF
PATH=\$PATH:/opt/hadoop-1.2.1/bin
export PATH

EOF

ssh-keygen -t dsa -P '' -f ~/.ssh/id_dsa
cat ~/.ssh/id_dsa.pub >> ~/.ssh/authorized_keys
/opt/hadoop-1.2.1/bin/hadoop namenode -format

echo Done: Hadoop initialization
SCRIPT

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  config.vm.box = "ubuntu/trusty64"
  config.vm.box_url = "http://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-amd64-vagrant-disk1.box"  
  config.vm.define :master do |master|
    master.vm.provider "virtualbox" do |vb|
      vb.name = "hadoop-sandbox"
      vb.customize ["modifyvm", :id, "--memory", "2048"]
    end
    master.vm.network :private_network, ip: "10.211.55.100"
    master.vm.hostname = "master"
    master.vm.provision :shell, :inline => $init_script
    master.vm.provision :shell, :privileged => false, :inline => $user_script
  end
end
